#include <iostream>

void printMatrix(int** mat, int size) {
    int i, j;

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            std::cout << mat[j][i] << " ";
        }

        std::cout << std::endl;
    }

    std::cout << std::endl << std::endl;
}

void printMatrixColor(int** mat, int size) {
    int i, j;

    for (i = 0; i < size; i++) {
        for (j = i; j < size; j++) {
            if (mat[j][i] != 0) {
                std::cout << mat[j][i];
            }
        }
    }

    std::cout << std::endl;
}

int** createMatrix(int size) {
    int i;
    int** mat;
    mat = new int * [ size ];

    for (i = 0; i < size; i++) {
        mat[i] = new int [size];
    }

    return mat;
}

void deleteMatrix(int** mat, int size) {
    int i;

    for (i = 0; i < size; i++) {
        delete [] mat[i];
    }

    delete [] mat;
}

int** copyMatrix(int** mat, int size) {
    int** matToReturn = createMatrix(size);

    int i, j;

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            matToReturn[i][j] = mat[i][j];
        }
    }

    return matToReturn;
}
