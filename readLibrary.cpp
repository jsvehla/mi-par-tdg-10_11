#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

#include "readLibrary.h"
#include "matrix.h"

bool fexists(const char* filename) {
    std::ifstream ifile(filename);
    return ifile;
}

bool isNumber(char number) {
    if (number >= 48 || number <= 57) {
        return true;

    } else {
        return false;
    }
}

int charToInt(char number) {
    return number - 48;
}

int** readFile(const char* filename, int& size) {
    int** Matrix = NULL;
    bool firstLine = true;
    int x = 0;
    int y = 0;
    std::string line;
    std::ifstream myfile(filename);

    if (myfile.is_open()) {
        while (myfile.good()) {
            getline(myfile, line);

            if (firstLine) {
                size = atoi(line.c_str());
                Matrix = createMatrix(size);
                firstLine = false;

            } else {
                for (int unsigned i = 0; i < line.size(); i++) {
                    Matrix[x][y] = charToInt(line[i]);
                    x++;

                    if (x > size - 1) {
                        x = 0;
                        y++;
                    }
                }
            }
        }

        myfile.close();
    }

    return Matrix;
}
