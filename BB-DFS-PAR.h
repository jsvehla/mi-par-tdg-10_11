#ifndef BB_DFS_PAR_H_fdlahgnrjgpoacrf5er5
#define BB_DFS_PAR_H_fdlahgnrjgpoacrf5er5

/* includes */
#include <deque>

/* states */
#define ST_SEARCHING        100
#define ST_WORK_REQ         101
#define ST_INIT             102
#define ST_FOUND_SOLUTION   103
#define ST_END              104
//#define ST_INIT_WAIT       105
#define ST_DO_NOTHING       106


/* colors */
#define NOEDGE          0
#define NOTCOLORED      1
#define COLORA          2
#define COLORB          3

/* token */
#define TOKENB          500
#define TOKENW          501

/* stack structure */
struct ToStack {
    int color;
    int posX;
    int posY;
    int countColorA;
    int countColorB;

    int * serialize() {
        int* array = new int[5];
        array[0] = color;
        array[1] = posX;
        array[2] = posY;
        array[3] = countColorA;
        array[4] = countColorB;
        return array;
    }

    ToStack deserialize(int* array) {
        color = array[0];
        posX = array[1];
        posY = array[2];
        countColorA = array[3];
        countColorB = array[4];
    }
};

/* method declarations */
int** DFSSearchPar(int**& mat, int& size, int workerNumber, int workerCount);
/* returns true if found better solution in step */
bool doBTStep(std::deque<ToStack>& st, int** mat, int size, int countEdges, int& bestSolution, int** & bestMat);

/* push next possible to stack */
void pushNext(std::deque<ToStack>& st, int nextX, int nextY, int countColorA, int countColorB);
/* boolean value (TRUE) if the branch is perspective and can find better solution */
bool cutBranch(int countEdges, int bestSolution, int countColorA, int countColorB);
/* count the edges in the graph */
int numberOfEdges(int** mat, int size);
/* find a triangle. ends with X index - only preceding edges (uncolored) */
bool isCollision(int** mat, int size, int posX, int posY, int color);
/* set color in graph, diagonally mirrored */
void setColor(int** mat, int X, int Y, int Color);
/* finds next edge, false if last. [-1,0] for search from first */
bool getNext(int** mat, int size, int curX, int curY, int& nextX, int& nextY);
/* finds the ID of the next processor in a row*/
int nextProcessor(int count, int current);
#endif // BB_DFS_PAR_H_fdlahgnrjgpoacrf5er5
