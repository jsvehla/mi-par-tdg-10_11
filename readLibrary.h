#ifndef READLIBRARY_H_INCLUDED
#define READLIBRARY_H_INCLUDED

//reads input file and return regular matrix
int** readFile(const char* filename, int& size);
//check if file exists
bool fexists(const char* filename);

// checks if char is number
bool isNumber(char number);
// translates char to integer
int charToInt(char number);

#endif // READLIBRARY_H_INCLUDED
