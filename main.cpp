#include "readLibrary.h"
#include "matrix.h"
#include "BB-DFS-PAR.h"
#include "mpi.h"

int main(int argc, char* argv[]) {
    int** mat;
    int** solution;
    int size;

    double time1, time2;

    int workerNumber;
    int workerCount;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &workerNumber);
    MPI_Comm_size(MPI_COMM_WORLD, &workerCount);

    MPI_Barrier(MPI_COMM_WORLD);
    time1 = MPI_Wtime();

    // correct arguments run
    if (argc == 2) {
        if (fexists(argv[1])) {
            // master will process matrix
            if (workerNumber == 0) {
                mat = readFile(argv[1], size);

            }
            // file does not exist, kill (should be send end
        } else {
            MPI_Finalize();
            return -1;
        }

        solution = DFSSearchPar(mat, size, workerNumber, workerCount);

        if (solution == NULL) {
            if (workerNumber == 0) {
                std::cout << "reseni neexistuje" << std::endl;
            }
        } else {
            if (workerNumber == 0) {
                std::cout << "reseni " << workerCount << " procesy: " << std::endl;
                printMatrix(solution, size);
            }
            deleteMatrix(solution, size);
        }

        deleteMatrix(mat, size);

        MPI_Barrier(MPI_COMM_WORLD);
        time2 = MPI_Wtime();

        if (workerNumber == 0) {
            std::cout << "Doba reseni: " << (time2 - time1) << std::endl;
        }


        // wrong arguments end
    } else {
        std::cout << "fuuuuuuj" << std::endl;
        MPI_Finalize();
        return -2;
    }

    MPI_Finalize();
    return 0;
}
