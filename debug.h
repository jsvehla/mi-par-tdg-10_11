#ifndef DEBUG_H_DSKHkjdfhgkjdsfhgkdf
#define DEBUG_H_DSKHkjdfhgkjdsfhgkdf

/**
Include file for debugging functions.

Use defire DEBUG_FUNC(x) for functions used only in debug builds. <iostream>, <string> for printing and "matrix.h" for matrix printing already included.
 */


#ifdef _DEBUG

#include <iostream>
#include <string>

#define DEBUG_FUNC_L(x) x

// level 2 debug
#ifdef _DEBUG_FULL
#define DEBUG_FUNC(x) x
#else
#define DEBUG_FUNC(x)
#endif

#else

#define DEBUG_FUNC(x)
#define DEBUG_FUNC_L(x)

#endif

#endif //DEBUG_H_DSKHkjdfhgkjdsfhgkdf
