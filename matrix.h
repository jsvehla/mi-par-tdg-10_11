#ifndef Matrix_H_ghaofdhkljdfgkjdfhgkjdfhkj
#define Matrix_H_ghaofdhkljdfgkjdfhgkjdfhkj

// prints whole matrix
void printMatrix(int** mat, int size);
// prints only colored edges
void printMatrixColor(int** mat, int size);

// allocates regular matrix
int** createMatrix(int size);
// deallocates regular matrix
void deleteMatrix(int** mat, int size);

int** copyMatrix(int** mat, int size);

#endif //Matrix_H_ghaofdhkljdfgkjdfhgkjdfhkj
