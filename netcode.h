#ifndef NETCODE_H_fdlagfhsdlfh5655dfgd
#define NETCODE_H_fdlagfhsdlfh5655dfgd


#define MSG_WORK_REQ            200
#define MSG_NO_WORK             201
#define MSG_WORK                202
#define MSG_SOLUTION            203
#define MSG_TOKEN               204
#define MSG_END                 205
#define MSG_INIT                208
#define MSG_INIT_1              209


#include <deque>
#include "BB-DFS-PAR.h"

#ifdef _DEBUG
extern int workerMine;
#endif

void sendWorkRequest(int dest);
void receiveWorkRequest();

void sendNoWork(int dest);
void receiveNoWork();

void sendWork(std::deque<ToStack>& btstack, int** mat, int dest, int matLength);
void receiveWork(std::deque<ToStack>& btstack, int** mat, int matLength) ;

void sendBestSolution(int** mat, int matLength, int dest, int bestSolution);
void receiveBestSolution(int** mat, int matLength, int &bestSolution);

void sendToken(int tokenColor, int dest);
void recieveToken(int &tokenColor);

void sendEnd(int dest);
void receiveEnd();

void sendInit(int** mat, int size, int countEdges, int dest);
void receiveInit(int**& mat, int& size, int& countEdges);
#endif //NETCODE_H_fdlagfhsdlfh5655dfgd
