#include "BB-DFS-PAR.h"
#include "netcode.h"
#include "matrix.h"

#include "debug.h"
#include <cstdlib>

#include "mpi.h"

#define MESSAGE_CHECK   100

int** DFSSearchPar(int**& mat, int& size, int workerNumber, int workerCount) {
    //MPI variables
    int flag;
    MPI_Status status;

    //token
    int endingTokenColor = TOKENB;
    int processColor = TOKENB;
    bool hasToken = false;

#ifdef _DEBUG
    workerMine = workerNumber;
#endif

    // number of worker to send next request
    int destNext = 0;

    //backtrack stack
    std::deque<ToStack> BTStack;

    // best solution data
    int bestSolution = size;
    int countEdges = 0; //numberOfEdges(mat, size);
    int** bestMat = NULL;

    int state = ST_DO_NOTHING;

    //initial worker
    if (workerNumber == 0) {
        state = ST_INIT;
        hasToken = true;
    }

    //main loop
    bool running = true;
    unsigned int loopCnt = 0;
    while (running) {
        loopCnt++;

        //check msg
        if ((loopCnt % MESSAGE_CHECK) == 0) {
            MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);

            if (flag) {
                switch (status.MPI_TAG) {

                        //work request response
                    case MSG_WORK_REQ:
                    {
                        receiveWorkRequest();

                        if (BTStack.empty()) {
                            sendNoWork(status.MPI_SOURCE);
                        } else {
                            if (workerNumber > status.MPI_SOURCE) {
                                processColor = TOKENB;
                            }
                            sendWork(BTStack, mat, status.MPI_SOURCE, size);
                        }

                        break;
                    }

                        //NO_WORK receive
                    case MSG_NO_WORK:
                    {
                        receiveNoWork();

                        //ask next
                        state = ST_WORK_REQ;
                        break;
                    }

                        //receive work
                    case MSG_WORK:
                    {
                        receiveWork(BTStack, mat, size);
                        state = ST_SEARCHING;
                        break;
                    }

                        //receive new best solution
                    case MSG_SOLUTION:
                    {
                        bestMat = createMatrix(size);
                        receiveBestSolution(bestMat, size, bestSolution);
                        break;
                    }

                    case MSG_TOKEN:
                    {
                        recieveToken(endingTokenColor);
                        if (processColor == TOKENB) {
                            endingTokenColor = TOKENB;
                        }
                        hasToken = true;

                        if ((workerNumber == 0) && (endingTokenColor == TOKENW)) {
                            state = ST_END;
                        }
                        break;
                    }

                    case MSG_END:
                    {
                        receiveEnd();
                        state = ST_END;
                        break;
                    }

                        //slave, receive init data
                    case MSG_INIT:
                    {
                        receiveInit(mat, size, countEdges);
                        DEBUG_FUNC_L(std::cout << workerMine << ": INIT data: " << size << " " << countEdges << std::endl;)
                        state = ST_SEARCHING;
                    }


                } //end tag
            } //end flag
        } //end msg check

        //state machine
        switch (state) {

                // initial
            case ST_INIT:
            {
                // prepare matrix, send
                countEdges = numberOfEdges(mat, size);
                for (int i = 1; i < workerCount; i++) {
                    sendInit(mat, size, countEdges, i);
                }

                int nextX, nextY;

                // find first edge and push it
                bool b = getNext(mat, size, -1, 0, nextX, nextY);

                // bad graph
                if (b == false) {
                    DEBUG_FUNC_L(std::cout << workerNumber << ": NO EDGES FOUND" << std::endl;)
                    return NULL;
                }

                pushNext(BTStack, nextX, nextY, 0, 0);



                state = ST_SEARCHING;
                break;
            }

                //			case INIT_WAIT: {
                //				// wait for work assignment
                //			}

                // search loop
            case ST_SEARCHING:
            {
                //empty stack we wait.
                if (BTStack.empty()) {
                    state = ST_WORK_REQ;
                } else if (doBTStep(BTStack, mat, size, countEdges, bestSolution, bestMat)) {
                    state = ST_FOUND_SOLUTION;
                }

                break;
            }

                // no work, send request
            case ST_WORK_REQ:
            {
                if (hasToken) {
                    if (workerNumber == 0) {
                        endingTokenColor = TOKENW;
                    }
                    sendToken(endingTokenColor, nextProcessor(workerCount, workerNumber));
                    processColor = TOKENW;
                    hasToken = false;
                }
                sendWorkRequest(destNext);
                destNext = (destNext + 1) % workerCount;
                if (destNext == workerNumber) destNext = (destNext + 1) % workerCount;; //skip self

                //switch to do nothing state and wait for response
                state = ST_DO_NOTHING;
                break;
            }

            case ST_DO_NOTHING:
            {
                // do nothing
                break;
            }

                // found better solution, notify
            case ST_FOUND_SOLUTION:
            {
                DEBUG_FUNC_L(std::cout << workerNumber << ": found solution: " << bestSolution << std::endl);


                //notify all
                for (int cnt = 0; cnt < workerCount; cnt++) {
                    if (cnt == workerNumber) continue; //skip self

                    sendBestSolution(bestMat, size, cnt, bestSolution);
                }

                // found best possible, stop
                if ((bestSolution == 0) || ((countEdges % 2 == 1) && (bestSolution == 1))) {
                    if (workerNumber != 0) {
                        sendEnd(0);
                    }
                    state = ST_END;
                } else {
                    state = ST_SEARCHING;
                }


                break;
            }

            case ST_END:
            {
                if (workerNumber == 0) {
                    for (int i = 1; i < workerCount; i++) {
                        sendEnd(i);
                    }
                }
                running = false;

                break;
            }
        }
    }

    return bestMat;
}

bool doBTStep(std::deque<ToStack>& st, int** mat, int size, int countEdges, int& bestSolution, int** & bestMat) {
    int nextX, nextY;

    ToStack t = st.front();
    st.pop_front();

    DEBUG_FUNC(std::cout << "processing -- x:" << t.posX << " y:" << t.posY << " col:" << t.color << std::endl;)

    setColor(mat, t.posX, t.posY, t.color);

    //possible mid-solution, not collision
    if (!isCollision(mat, size, t.posX, t.posY, t.color)) {
        // no more edges, graph done
        if (!getNext(mat, size, t.posX, t.posY, nextX, nextY)) {
            int curSolution = std::abs(t.countColorA - t.countColorB);

            // current solution is better than current best
            if (curSolution < bestSolution) {
                bestSolution = curSolution;

                if (bestMat != NULL) {
                    deleteMatrix(bestMat, size);
                }

                bestMat = copyMatrix(mat, size);

                return true;
            }

            //more edges
        } else {

            // searched branch can still have a better than current solution
            if (!cutBranch(countEdges, bestSolution, t.countColorA, t.countColorB)) {
                // push next
                pushNext(st, nextX, nextY, t.countColorA, t.countColorB);

                // can't, cut it
            } else {
                DEBUG_FUNC(std::cout << "cutting branch: ";)
                DEBUG_FUNC(printMatrixColor(mat, size);)
                DEBUG_FUNC(std::cout << std::endl;)
            }
        }
    }

    return false;
}

void pushNext(std::deque<ToStack>& st, int nextX, int nextY, int countColorA, int countColorB) {
    ToStack t1;
    ToStack t2;

    t1.color = COLORA;
    t1.posX = nextX;
    t1.posY = nextY;
    t1.countColorA = countColorA + 1;
    t1.countColorB = countColorB;

    t2.color = COLORB;
    t2.posX = nextX;
    t2.posY = nextY;
    t2.countColorA = countColorA;
    t2.countColorB = countColorB + 1;

    if (countColorA > countColorB) {
        st.push_front(t1);
        st.push_front(t2);

    } else {
        st.push_front(t2);
        st.push_front(t1);
    }
}

bool isCollision(int** mat, int size, int posX, int posY, int color) {
    for (int i = 0; i < posY; i++) {
        if ((mat[i][posY] == color) && (mat[i][posX] == color)) {

            DEBUG_FUNC(std::cout << "Collision found... ";)
            DEBUG_FUNC(printMatrixColor(mat, size);)
            DEBUG_FUNC(std::cout << std::endl;)

            return true;
        }
    }

    return false;
}

void setColor(int** mat, int xx, int yy, int color) {
    mat[xx][yy] = color;
    mat[yy][xx] = color;
}

bool getNext(int** mat, int size, int curX, int curY, int& nextX, int& nextY) {

    while (true) {
        curX++;

        //column end
        if (curX >= size) {
            curY++;
            curX = curY;
        }

        //row end, end of graph
        if (curY >= size) {
            return false;
        }

        if (mat[curX][curY] != NOEDGE) {
            nextX = curX;
            nextY = curY;
            return true;
        }
    }
}

int numberOfEdges(int** mat, int size) {
    int count = 0;

    for (int i = 0; i < size; i++) {
        for (int j = i; j < size; j++) {
            if (mat[i][j] >= NOTCOLORED) {
                count++;
            }
        }
    }
    return count;
}

bool cutBranch(int countEdges, int bestSolution, int countColorA, int countColorB) {
    int restBranches = countEdges - countColorA - countColorB;
    int solution = (abs(countColorA - countColorB));

    if (solution > restBranches) {
        if ((solution - restBranches) > bestSolution) {
            return true;
        }
    }

    return false;
}

int nextProcessor(int count, int current) {
    if (current == count - 1) {
        return 0;
    }
    return current + 1;
}