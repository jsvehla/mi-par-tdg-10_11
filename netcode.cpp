#include "netcode.h"
#include "debug.h"
#include "mpi.h"
#include "matrix.h"

#ifdef _DEBUG
int workerMine = 0;
#endif

void sendToken(int tokenColor, int dest) {
    DEBUG_FUNC_L(std::cout << workerMine << ": Token send " << dest << std::endl;)

            int msg = tokenColor;
    MPI_Send(&msg, 1, MPI_INT, dest, MSG_TOKEN, MPI_COMM_WORLD);
}

void sendWorkRequest(int dest) {
    DEBUG_FUNC_L(std::cout << workerMine << ": WORK_REQ send " << dest << std::endl;)

            int msg = 0;
    MPI_Send(&msg, 1, MPI_INT, dest, MSG_WORK_REQ, MPI_COMM_WORLD);
}

void sendNoWork(int dest) {
    DEBUG_FUNC_L(std::cout << workerMine << ": NO_WORK send " << dest << std::endl;)

            int msg = 0;
    MPI_Send(&msg, 1, MPI_INT, dest, MSG_NO_WORK, MPI_COMM_WORLD);
}

void receiveWorkRequest() {
    DEBUG_FUNC_L(std::cout << workerMine << ": WORK_REQ receive" << std::endl;)
            int msg = 0;
    MPI_Status status;

    MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, MSG_WORK_REQ, MPI_COMM_WORLD, &status);
}

void receiveNoWork() {
    DEBUG_FUNC_L(std::cout << workerMine << ": NO_WORK receive" << std::endl;)
            int msg = 0;
    MPI_Status status;

    MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, MSG_NO_WORK, MPI_COMM_WORLD, &status);
}

void sendWork(std::deque<ToStack>& btstack, int** mat, int dest, int matLength) {
    DEBUG_FUNC_L(std::cout << workerMine << ": WORK send " << dest << std::endl;)
    ToStack stack;
    stack = btstack.back();
    int * array = stack.serialize();
    btstack.pop_back();

    int arrayMat [matLength * matLength + 5];
    for (int i = 0; i < matLength; i++) {
        for (int j = 0; j < matLength; j++) {
            arrayMat[i * matLength + j] = mat[i][j];
        }
    }
    for (int i = 0; i < 5; i++) {
        arrayMat[matLength * matLength + i] = array[i];
    }
    MPI_Send(arrayMat, matLength * matLength + 5, MPI_INT, dest, MSG_WORK, MPI_COMM_WORLD);
    delete[] array;
}

void receiveWork(std::deque<ToStack>& btstack, int** mat, int matLength) {
    DEBUG_FUNC_L(std::cout << workerMine << ": WORK receive" << std::endl;)

    MPI_Status status;
    ToStack stack;
    int buffer [matLength * matLength + 5];
    int array[5];

    MPI_Recv(&buffer, matLength * matLength + 5, MPI_INT, MPI_ANY_SOURCE, MSG_WORK, MPI_COMM_WORLD, &status);

    for (int i = 0; i < matLength; i++) {
        for (int j = 0; j < matLength; j++) {
            mat[i][j] = buffer[i * matLength + j];
        }
    }
    for (int i = 0; i < 5; i++) {
        array[i] = buffer[matLength * matLength + i];
    }
    stack.deserialize(array);
    btstack.push_front(stack);
}

void sendBestSolution(int** mat, int matLength, int dest, int bestSolution) {
    DEBUG_FUNC_L(std::cout << workerMine << ": Sending best solution " << dest << std::endl;)

            int arrayMat [matLength * matLength + 1];
    for (int i = 0; i < matLength; i++) {
        for (int j = 0; j < matLength; j++) {
            arrayMat[i * matLength + j] = mat[i][j];
        }
    }

    arrayMat[matLength * matLength] = bestSolution;
    MPI_Send(arrayMat, matLength * matLength + 1, MPI_INT, dest, MSG_SOLUTION, MPI_COMM_WORLD);
}

void receiveBestSolution(int** mat, int matLength, int &bestSolution) {
    DEBUG_FUNC_L(std::cout << workerMine << ": Best solution received" << std::endl;)


    MPI_Status status;
    int buffer [matLength * matLength + 1];

    MPI_Recv(&buffer, matLength * matLength + 1, MPI_INT, MPI_ANY_SOURCE, MSG_SOLUTION, MPI_COMM_WORLD, &status);

    for (int i = 0; i < matLength; i++) {
        for (int j = 0; j < matLength; j++) {
            mat[i][j] = buffer[i * matLength + j];
        }
    }

    bestSolution = buffer[matLength * matLength];
}

void recieveToken(int &tokenColor) {
    DEBUG_FUNC_L(std::cout << workerMine << ": TOKEN receive" << std::endl;)

            int msg;
    MPI_Status status;

    MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, MSG_TOKEN, MPI_COMM_WORLD, &status);
    tokenColor = msg;
}

void sendEnd(int dest) {
    DEBUG_FUNC_L(std::cout << workerMine << ": END send " << dest << std::endl;)

            int msg = 0;
    MPI_Send(&msg, 1, MPI_INT, dest, MSG_END, MPI_COMM_WORLD);
}

void receiveEnd() {
    DEBUG_FUNC_L(std::cout << workerMine << ": END receive" << std::endl;)

            int msg;
    MPI_Status status;

    MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, MSG_END, MPI_COMM_WORLD, &status);
}

void sendInit(int** mat, int size, int countEdges, int dest) {
    DEBUG_FUNC_L(std::cout << workerMine << ": INIT send " << dest << std::endl;)

            int buffer [size * size + 1];
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            buffer[i * size + j] = mat[i][j];
        }
    }

    buffer[size * size ] = countEdges;

    int msg = size;
    MPI_Send(&msg, 1, MPI_INT, dest, MSG_INIT, MPI_COMM_WORLD);
    MPI_Send(buffer, size * size + 1, MPI_INT, dest, MSG_INIT_1, MPI_COMM_WORLD);
}

void receiveInit(int**& mat, int& size, int& countEdges) {
    DEBUG_FUNC_L(std::cout << workerMine << ": INIT receive" << std::endl;)

    MPI_Status status;
    int msg = 0;
    MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, MSG_INIT, MPI_COMM_WORLD, &status);
    size = msg;

    int* buffer = new int[size * size + 1];
    MPI_Recv(buffer, size * size + 1, MPI_INT, MPI_ANY_SOURCE, MSG_INIT_1, MPI_COMM_WORLD, &status);

        mat = createMatrix(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                mat[i][j] = buffer[i * size + j];
            }
        }
    countEdges = buffer[size * size];

    delete[] buffer;
}
